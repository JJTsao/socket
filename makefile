CC = gcc

CFLAGS = -g -Wall

TARGET = ./clientA ./clientB ./central/central ./serverT/serverT ./serverS/serverS ./serverP/serverP

.PHONY: serverC serverT serverS serverP

all:
	$(CC) $(CFLAGS) -o ./clientA ./clientA.c
	$(CC) $(CFLAGS) -o ./clientB ./clientB.c
	$(CC) $(CFLAGS) -o ./central/central ./central/central.c
	$(CC) $(CFLAGS) -o ./serverT/serverT ./serverT/serverT.c
	$(CC) $(CFLAGS) -o ./serverS/serverS ./serverS/serverS.c
	$(CC) $(CFLAGS) -o ./serverP/serverP ./serverP/serverP.c

serverC:
	./central/central

serverT:
	./serverT/serverT

serverS:
	./serverS/serverS

serverP:
	./serverP/serverP

clean:
	$(RM) $(TARGET)