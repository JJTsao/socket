#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

const int SERVER_T_UDP_PORT = 21091;
const int SERVER_S_UDP_PORT = 22091;
const int SERVER_P_UDP_PORT = 23091;
const int USER_COUNT = 6;

struct User {
    char name[64];
    struct User *neighbor;
};

struct UserNode {
    struct User user;
    struct UserNode *next;
};

struct Vertex {
    char name[64];
    bool visited;
    struct Vertex *next;
};

struct UserNode *user_first_node = NULL;
struct Vertex   *vertex_list;
// int user_count;

bool is_userlist_empty();
struct UserNode* get_last_user();
struct UserNode* get_userNode_by_name(char* name);
void add_new_neighbor(struct UserNode *user, struct User *neighbor);
void add_new_userNode(struct UserNode *new_user);
int show_graph();

struct Vertex* get_last_vertex();
void vertex_list_init();
void DFS(char* name);

int udp_server_socket();