#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdbool.h>
#include "serverT.h"

// user_first_node = NULL;

bool is_userlist_empty() { return (user_first_node == NULL); }

struct UserNode* get_last_user() {
    struct UserNode *tail = user_first_node;
    if(tail == NULL) return NULL;
    while(tail->next != NULL) { tail = tail->next; }
    return tail;
}

struct UserNode* get_userNode_by_name(char* name) {
    if(user_first_node == NULL) return NULL;
    struct UserNode *cur = user_first_node;
    while(cur != NULL) {
        if(strncmp(name, cur->user.name, 64) == 0) {
            return cur;
        }
        cur = cur->next;
    }
    return NULL;
}

void add_new_neighbor(struct UserNode *user, struct User *neighbor) {
    struct User *cur_nbr = user->user.neighbor;
    while(cur_nbr->neighbor != NULL) { cur_nbr = cur_nbr->neighbor; }
    cur_nbr->neighbor = neighbor;
    return;
}

void add_new_userNode(struct UserNode *new_user) {
    if(is_userlist_empty()) user_first_node = new_user;
    else {
        struct UserNode *tail = get_last_user();
        tail->next = new_user;
    }
}

int show_graph() { // now return the size of users
    struct UserNode *now = user_first_node;
    int size = 0;
    while(now != NULL) {
        printf("%s:\n", now->user.name);
        struct User *cur_nbr = now->user.neighbor;
        while(cur_nbr != NULL) {
            printf("\t%s", cur_nbr->name);
            cur_nbr = cur_nbr->neighbor;
        }
        printf("\n");
        now = now->next;
        size++;
    }
    return size;
}

bool is_vertexlist_empty() { return (vertex_list == NULL); }

struct Vertex* get_last_vertex() {
    struct Vertex *tail = vertex_list;
    if(tail == NULL) return NULL;
    while(tail->next != NULL) { tail = tail->next; }
    return tail;
}

void add_new_vertex(struct Vertex *new_vertex) {
    if(is_vertexlist_empty()) vertex_list = new_vertex;
    else {
        struct Vertex *tail = get_last_vertex();
        tail->next = new_vertex;
    }
}

void vertex_list_init() {
    struct UserNode *cur_user = user_first_node;
    while(cur_user != NULL) {
        struct Vertex *tmp_vertex = malloc(sizeof(*tmp_vertex));
        strncpy(tmp_vertex->name, cur_user->user.name, 64);
        tmp_vertex->visited = false;
        tmp_vertex->next = NULL;

        add_new_vertex(tmp_vertex);
        cur_user = cur_user->next; 
    }
}

void DFS(char* name) {
    // mark Vertex v where v.name == name as visited.
    struct Vertex *cur_vertex = vertex_list;
    while(cur_vertex != NULL) {
        if(!strcmp(cur_vertex->name, name)) { 
            if(cur_vertex->visited == true) return;
            else { cur_vertex->visited = true; break; }
        }
        cur_vertex = cur_vertex->next;
    }
    
    struct UserNode *cur_userNode = user_first_node;
    while(cur_userNode != NULL) {
        if(!strcmp(cur_userNode->user.name, name)) {
            struct User *cur_nbr = cur_userNode->user.neighbor;
            while(cur_nbr != NULL) {
                DFS(cur_nbr->name);
                cur_nbr = cur_nbr->neighbor;
            }
            break;
        }
        cur_userNode = cur_userNode->next;
    }
}

int udp_server_socket() {
    int socket_fd;
    struct sockaddr_in server;

    if( (socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("Socket Created Failed\n");
        exit(1);
    }

    memset(&server, '\0', sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(SERVER_T_UDP_PORT);

    if( bind(socket_fd, (struct sockaddr*)&server, sizeof(server)) < 0 ) {
        perror("Socket Binded Failed\n");
        exit(1);
    }
    return socket_fd;
}

int main() {
    FILE *fp;
    fp = fopen("/home/student/socket/serverT/edgelist.txt", "r");
    if(fp == NULL) {
        perror("fopen failed.");
        exit(1);
    }

    // Build the Graph in adjacency list data structure
    char arr[64];
    char *substr1, *substr2;
    while( fgets(arr, 64, fp) != NULL ) {
        // node 1 name, e.g. Racheal
        substr1 = strtok(arr, " ");
        // node 2 name, e.g. Victor
        substr2 = strtok(NULL, "\n");\
        
        struct UserNode *user_node;
        // check if username "substr1" node exist
        if( (user_node = get_userNode_by_name(substr1)) != NULL) { // user exist
            struct User *temp = malloc(sizeof(*temp));
            strncpy(temp->name, substr2, 64);
            temp->neighbor = NULL;

            add_new_neighbor(user_node, temp);
        } else { // user doesn't exist
            // new Node substr1
            struct UserNode *tmpNode = malloc(sizeof(*tmpNode));
            strncpy(tmpNode->user.name, substr1, 64);\
            // set neighbor substr2 of substr1 Node
            struct User *temp = malloc(sizeof(*temp));
            strncpy(temp->name, substr2, 64);
            temp->neighbor = NULL;
            tmpNode->user.neighbor = temp;
            
            add_new_userNode(tmpNode);
        }
        // check if username "substr2" node exist
        if( (user_node = get_userNode_by_name(substr2)) != NULL) { // user exist
            struct User *temp = malloc(sizeof(*temp));
            strncpy(temp->name, substr1, 64);
            temp->neighbor = NULL;

            add_new_neighbor(user_node, temp);
        } else { // user doesn't exist
            // new Node substr2
            struct UserNode *tmpNode = malloc(sizeof(*tmpNode));
            strncpy(tmpNode->user.name, substr2, 64);\
            // set neighbor substr1 of substr2 Node
            struct User *temp = malloc(sizeof(*temp));
            strncpy(temp->name, substr1, 64);
            temp->neighbor = NULL;
            tmpNode->user.neighbor = temp;
            
            add_new_userNode(tmpNode);
        }
    }
    vertex_list_init();

    // socket create & bind
    int server_t_socket_fd = udp_server_socket();
    printf("The ServerT is up and running using UDP on port %d.\n", SERVER_T_UDP_PORT);
    
    while(1) {
        // socket wait for data 
        char buf[256];
        struct sockaddr_in client_addr;
        int length = sizeof(client_addr);
        if( recvfrom(server_t_socket_fd, &buf, 256, 0, (struct sockaddr*)&client_addr, (socklen_t*)&length) < 0 ) {
            perror("couldn't read datagram\n");
        }
        printf("The ServerT received a request from Central to get the topology.\n");

        DFS(buf);
        
        struct UserNode *tar = user_first_node;
        struct Vertex   *v   = vertex_list;
        while(tar != NULL) {
            if(v->visited == 1) {
                if(sendto(server_t_socket_fd, &tar->user.name, 64, 0, (struct sockaddr*)&client_addr, length) < 0) {
                    perror("Could not send datagram!!\n");
                }
                struct User *nbr = tar->user.neighbor;
                // send the neighbors of the main user
                while(nbr != NULL) {
                    sendto(server_t_socket_fd, " ", 1, 0, (struct sockaddr*)&client_addr, length);
                    if(sendto(server_t_socket_fd, &nbr->name, 64, 0, (struct sockaddr*)&client_addr, length) < 0) {
                        perror("Could not send datagram!!\n");
                    }
                    nbr = nbr->neighbor;
                }
                // send a following '\n' as an adjacency list finished
                if(sendto(server_t_socket_fd, "\n", 1, 0, (struct sockaddr*)&client_addr, length) < 0) {
                    perror("Could not send datagram!!\n");
                }
            }
            tar = tar->next;
            v   = v->next;
        }
        // all data send to serverC done with a following '0'
        if(sendto(server_t_socket_fd, "0\n", 2, 0, (struct sockaddr*)&client_addr, length) < 0) {
            perror("Could not send datagram!!\n");
        }
        printf("The ServerT finished sending the topology to Central.\n");
    }
    // close(server_t_socket_fd);
    fclose(fp);
    return 0;
}