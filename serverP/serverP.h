#include <stdio.h>
#include <stdbool.h>

const int SERVER_T_UDP_PORT = 21091;
const int SERVER_S_UDP_PORT = 22091;
const int SERVER_P_UDP_PORT = 23091;

struct User {
    char name[64];
    struct User *neighbor;
};

struct UserNode {
    int index;
    struct User user;
    struct UserNode *next;
};

struct ScoreList {
    char username[64];
    int score;
};

struct Queue {
    int *arr; // need to be init memory size before use
    int capacity;
    int front;
    int back;
};

struct UserNode *user_first_node = NULL;
int user_count = 0;

bool is_userlist_empty();
struct UserNode* get_last_user();
struct UserNode* get_userNode_by_name(char* name);
void add_new_neighbor(struct UserNode *user, struct User *neighbor);
void add_new_userNode(struct UserNode *new_user);

bool queue_is_empty(struct Queue *queue);
int queue_front(struct Queue *queue);
void queue_pop(struct Queue *queue);
void queue_push(struct Queue *queue, int elem);
int* BFS(int userA_idx, int userB_idx); // return predecessor array for the path info 

bool is_compatible(char* username_A, char* username_B);
float matching_gap(struct ScoreList *score_list, int userA_idx, int userB_idx);
char* get_name_on_path(int* predecessor, struct ScoreList *score_list, int userA_idx, int userB_idx);
float get_total_on_path(int* predecessor, struct ScoreList *score_list, int userA_idx, int userB_idx);

int udp_server_socket();
