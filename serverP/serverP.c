#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdbool.h>
#include <math.h>
#include "serverP.h"

bool is_userlist_empty() { return (user_first_node == NULL); }

struct UserNode* get_last_user() {
    struct UserNode *tail = user_first_node;
    if(tail == NULL) return NULL;
    while(tail->next != NULL) { tail = tail->next; }
    return tail;
}

struct UserNode* get_userNode_by_name(char* name) {
    if(user_first_node == NULL) return NULL;
    struct UserNode *cur = user_first_node;
    while(cur != NULL) {
        if(strncmp(name, cur->user.name, 64) == 0) {
            return cur;
        }
        cur = cur->next;
    }
    return NULL;
}

void add_new_neighbor(struct UserNode *user, struct User *neighbor) {
    struct User *cur_nbr = user->user.neighbor;
    if(cur_nbr == NULL) { user->user.neighbor = neighbor; return; }
    while(cur_nbr->neighbor != NULL) { cur_nbr = cur_nbr->neighbor; }
    cur_nbr->neighbor = neighbor;
    return;
}

void add_new_userNode(struct UserNode *new_user) {
    if(is_userlist_empty()) user_first_node = new_user;
    else {
        struct UserNode *tail = get_last_user();
        tail->next = new_user;
    }
}

bool queue_is_empty(struct Queue *queue) { return (queue->front == queue->back); }

int queue_front(struct Queue *queue) { if(queue_is_empty(queue)) return -1; else return queue->arr[(queue->front + 1) % queue->capacity]; }

void queue_pop(struct Queue *queue) {
    if(queue_is_empty(queue)) { printf("Queue is empty.\n"); return; }
    queue->front += 1; queue->front %= queue->capacity;
}

void queue_push(struct Queue *queue, int elem) {
    queue->back += 1; queue->back %= queue->capacity;
    queue->arr[queue->back] = elem;
}

int* BFS(int userA_idx, int userB_idx) {
    int color[user_count];
    int distance[user_count]; // maybe additional
    int *predecessor = malloc(user_count * sizeof(int));
    
    /** Initialization **/
    struct Queue *queue = malloc(sizeof(*queue));
    queue->arr = malloc(user_count * sizeof(int));
    memset(queue->arr, -1, user_count * sizeof(int));
    queue->capacity = user_count; queue->front = 0; queue->back = 0;
    for(int i=0; i<user_count; i++) {
        color[i] = 0; // 0: white, 1: gray, 2: black
        distance[i] = user_count - 1 ; // init dis with max value (user_count - 1)
        predecessor[i] = -1; // -1: no predecessor, n: user[n]
    }

    /** Main Algorithm **/
    color[userA_idx] = 1;
    distance[userA_idx] = 0;
    predecessor[userA_idx] = -1;
    queue_push(queue, userA_idx);
    while(!queue_is_empty(queue)) {
        int u = queue_front(queue);
        struct UserNode *iter = user_first_node;
        while(iter != NULL) {
            if(iter->index != u) { // Linked List find QQ
                iter = iter->next;
                continue;
            }

            struct User *cur_nbr = iter->user.neighbor;
            while(cur_nbr != NULL) {
                int tmp_idx; struct UserNode *tmp = user_first_node;
                while(tmp != NULL) { // get the current neighbor's index ....
                    if(!strcmp(cur_nbr->name, tmp->user.name)) {
                        tmp_idx = tmp->index;
                        break;
                    }
                    tmp = tmp->next;
                }
                if(color[tmp_idx] == 0) {   // if the color of vertex "tmp_idx" is white
                    color[tmp_idx] = 1;     // turn it to gray, which means it is been found
                    distance[tmp_idx] = distance[u] + 1;    // dis is the dis of predecessor "u" + 1
                    predecessor[tmp_idx] = u;   // predecessor is u
                    queue_push(queue, tmp_idx); // push vertex into queue
                    // if(!queue_is_empty(queue)) printf("front: %d\n", queue_front(queue));
                }
                cur_nbr = cur_nbr->neighbor;
            }
            iter = iter->next;
        }

        queue_pop(queue);   // pop u
        color[u] = 2;       // turn "u" to black
    }
    return predecessor;
}

bool is_compatible(char* username_A, char* username_B) {
    struct UserNode *tmpNode = user_first_node;
    while(tmpNode != NULL) {
        if(!strcmp(username_B, tmpNode->user.name)) return true;
        tmpNode = tmpNode->next;
    }
    return false;
}

float matching_gap(struct ScoreList *score_list, int userA_idx, int userB_idx) {
    float s1 = score_list[userA_idx].score;
    float s2 = score_list[userB_idx].score;
    return (abs(s1 - s2) / (s1 + s2));
}

char* get_name_on_path(int* predecessor, struct ScoreList *score_list, int userA_idx, int userB_idx) {
    int cur = userB_idx;
    char* result = malloc(0);
    while(cur != userA_idx) {
        cur = predecessor[cur];
        char *cur_name = score_list[cur].username;
        strncat(result, cur_name, 64);
        strncat(result, " ", 1);
    }
    return result;
}

float get_total_on_path(int* predecessor, struct ScoreList *score_list, int userA_idx, int userB_idx) {
    int cur = userB_idx, old;
    float total_matching_gap = 0;
    while(cur != userA_idx) {
        old = cur;
        cur = predecessor[cur];
        total_matching_gap += matching_gap(score_list, cur, old);
    }
    return total_matching_gap;
}

int udp_server_socket() {
    int socket_fd;
    struct sockaddr_in server;

    if( (socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("Socket Created Failed\n");
        exit(1);
    }
    // printf("Socket Created Success.\n");

    memset(&server, '\0', sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(SERVER_P_UDP_PORT);

    if( bind(socket_fd, (struct sockaddr*)&server, sizeof(server)) < 0 ) {
        perror("Socket Binded Failed\n");
        exit(1);
    }
    // printf("Socket Binded Success.\n");

    return socket_fd;
}

int main() {
    int server_p_socket_fd = udp_server_socket();
    printf("The ServerP is up and running using UDP on port %d.\n", SERVER_P_UDP_PORT);

    while(1) {
        char buf[256];
        struct sockaddr_in client_addr;
        int length = sizeof(client_addr);
        char username_A[64], username_B[64];
        recvfrom(server_p_socket_fd, &username_A, 256, 0, (struct sockaddr*)&client_addr, (socklen_t*)&length);
        recvfrom(server_p_socket_fd, &username_B, 256, 0, (struct sockaddr*)&client_addr, (socklen_t*)&length);

        if( recvfrom(server_p_socket_fd, &buf, 256, 0, (struct sockaddr*)&client_addr, (socklen_t*)&length) < 0 ) {
            perror("couldn't read datagram\n");
        }
        printf("The ServerP received the topology and score information.\n");
        /***** Calculate the user count *****/
        for(int i=0; i<sizeof(buf); i++) { if(buf[i] == '\n') user_count++; }
        user_count -= 2; user_count /= 2;
        struct ScoreList score_list[user_count];
        memset(score_list, '\0', sizeof(score_list));
        
        /***** Parse the recv data to build adjacency list & score array *****/
        int start = 0, end = 0;
        int state = 0;
        int usr_idx = 0, userA_idx, userB_idx, idx = 0;
        for(int i=0; i<sizeof(buf); i++) { 
            char tmp_line[256];
            memset(tmp_line, '\0', sizeof(tmp_line));
            struct UserNode *tmpNode = malloc(sizeof(*tmpNode));
            if(buf[i] == '\n') {
                end = i;
                strncpy(tmp_line, &buf[start], end - start);
                if(!strcmp(tmp_line, "0")) {
                    state = 1; start += 2;
                    continue;
                } else if(!strcmp(tmp_line, "1")) {
                    state = 2; start += 2;
                    break;
                }
                
                switch(state) {
                    case 0: // build adjacency list
                        ; struct User *main_user = malloc(sizeof(*main_user));
                        char *substr = strtok(tmp_line, " ");
                        strncpy(main_user->name, substr, 64);
                        main_user->neighbor = NULL;
                        tmpNode->index = idx;
                        tmpNode->user = *main_user;
                        tmpNode->next = NULL;
                        substr = strtok(NULL, " ");
                        while(substr != NULL) {
                            struct User *tmp_user = malloc(sizeof(*tmp_user));
                            strncpy(tmp_user->name, substr, 64);
                            tmp_user->neighbor = NULL;
                            add_new_neighbor(tmpNode, tmp_user);
                            substr = strtok(NULL, " ");
                        }
                        add_new_userNode(tmpNode);
                        idx++;
                        break;
                    case 1: { // build score arr
                        ; char *substr = strtok(tmp_line, " ");
                        strncpy(score_list[usr_idx].username, substr, 64);
                        substr = strtok(NULL, " ");
                        score_list[usr_idx].score = atoi(substr);
                        usr_idx++;
                        break;
                    }
                    default:
                        printf("Error occur!\n");
                }
                start = end + 1;
            }
        }
        if(!is_compatible(username_A, username_B)) {
            sendto(server_p_socket_fd, "", 0, 0, (struct sockaddr*)&client_addr, length);
            sendto(server_p_socket_fd, "0", 1, 0, (struct sockaddr*)&client_addr, length);
            // printf("Not compatible\n");
            continue;
        }

        // set userA_idx & userB_idx
        for(int i=0; i<user_count; i++) {
            if(!strcmp(score_list[i].username, username_A)) userA_idx = i;
            if(!strcmp(score_list[i].username, username_B)) userB_idx = i;
        }
        /***** Find matching gap between two user using BFS *****/
        int *predecessor = BFS(userA_idx, userB_idx);
        char result[256]; memset(result, '\0', sizeof(result));
        strncpy(result, username_B, 64); 
        strncat(result, " ", 1);

        strncat(result, get_name_on_path(predecessor, score_list, userA_idx, userB_idx), 256);
        char total_gap[64]; sprintf(total_gap, "%f", get_total_on_path(predecessor, score_list, userA_idx, userB_idx));
        // printf("result: %s\n", result);
        // printf("matcing gap: %s\n", total_gap);

        sendto(server_p_socket_fd, result, sizeof(result), 0, (struct sockaddr*)&client_addr, length);
        sendto(server_p_socket_fd, total_gap, sizeof(total_gap), 0, (struct sockaddr*)&client_addr, length);
        printf("The ServerP finished sending the results to the Central.\n");
    }
    // close(server_p_socket_fd);
    return 0;
}