#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <stdbool.h>
#include "central.h"

int udp_client_socket() {
    int fd;
    if( (fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("UDP Socket Created Failed\n");
        exit(1);
    }

    struct sockaddr_in self_addr;
    memset(&self_addr, '\0', sizeof(self_addr));
    self_addr.sin_family = AF_INET;
    self_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    self_addr.sin_port = htons(SERVER_C_UDP_PORT);

    if( bind(fd, (struct sockaddr*)&self_addr, sizeof(self_addr)) < 0 ) {
        perror("UDP Bind Failed\n");
        exit(1);
    }

    for(int i=0; i<3; i++) {
        memset(&server_addr[i], '\0', sizeof(server_addr));
        server_addr[i].sin_family = AF_INET;
        switch(i) {
            case 0: server_addr[i].sin_port = htons(SERVER_T_UDP_PORT); break;
            case 1: server_addr[i].sin_port = htons(SERVER_S_UDP_PORT); break;
            case 2: server_addr[i].sin_port = htons(SERVER_P_UDP_PORT); break;
        }
    }
    return fd;
}

int tcp_server_socket_setup(int port) {
    int socket_fd;

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd == -1) {
        perror("Socket Create Failed.\n");
        exit(1);
    }
    
    struct sockaddr_in server;
    memset(&server, '\0', sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(port);
    
    if( bind(socket_fd, (struct sockaddr*)&server, sizeof(server)) < 0 ) {
        perror("Socket Bind Failed.\n");
        exit(1);
    }
    listen(socket_fd, MAX_ACCEPT_SOCK_COUNT); 
    return socket_fd;
}

int tcp_accept_client_sock(int server_socket_fd) {
    struct sockaddr_in client;
    int client_sock;
    int c = sizeof(struct sockaddr_in);
    client_sock = accept(server_socket_fd, (struct sockaddr*)&client, (socklen_t*)&c);
    if(client_sock < 0) {
        perror("Socket Accept Failed.\n");
        exit(1);
    }
    return client_sock;
}

int main(int argc, char *argv[]) {
    // int tcp_socket_a_fd = tcp_server_socket(SERVER_C_TCP_PORT_A);
    // int tcp_socket_b_fd = tcp_server_socket(SERVER_C_TCP_PORT_B);
    int tcp_server_socket_fd_a = tcp_server_socket_setup(SERVER_C_TCP_PORT_A);
    int tcp_server_socket_fd_b = tcp_server_socket_setup(SERVER_C_TCP_PORT_B);
    int udp_socket_fd = udp_client_socket();

    printf("The Central server is up and running.\n");
    while(1) {
        char username_A[64], username_B[64];
        memset(username_A, '\0', sizeof(username_A));
        memset(username_B, '\0', sizeof(username_B));
        /***** Phase 1-1: Client A *****/
        int tcp_socket_a_fd = tcp_accept_client_sock(tcp_server_socket_fd_a);
        if( recv(tcp_socket_a_fd, username_A, 64, 0) < 0 ) { // get username_A
            perror("recv Failed.\n");
            exit(1);
        }
        printf("The Central server received input=%s from the client using TCP over port %d.\n", username_A, SERVER_C_TCP_PORT_A);
        
        /***** Phase 1-2: Client B *****/
        int tcp_socket_b_fd = tcp_accept_client_sock(tcp_server_socket_fd_b);
        if( recv(tcp_socket_b_fd, username_B, 64, 0) < 0 ) { // get username_B
            perror("recv Failed.\n");
            exit(1);
        }
        printf("The Central server received input=%s from the client using TCP over port %d.\n", username_B, SERVER_C_TCP_PORT_B);

        /***** Phase 2-1: Server T, send username_A & username_B to server T *****/
        if( sendto(udp_socket_fd, username_A, 64, 0, (struct sockaddr*)&server_addr[0], length) < 0 ) { // send username_A to serverT
            perror("write to server error!\n");
            exit(1);
        }
        printf("The Central server sent a request to Backend-Server T\n");

        /***** Phase 2-2: Server T, recv topology of user_A from server T *****/
        char stored_data[1024];
        memset(stored_data, '\0', sizeof(stored_data));

        char response_T[256];
        char *userlist = malloc(1 * sizeof(char));
        bool main_user = true;
        while(1) {
            memset(response_T, '\0', sizeof(response_T));
            // recv data from serverT
            if( recvfrom(udp_socket_fd, response_T, 256, 0, (struct sockaddr*)&server_addr[0], (socklen_t*)&length) < 0) {
                perror("read from server error\n");
                exit(1);
            }
            strncat(stored_data, response_T, 256);
            // store the main users for later retransmit to serverS
            if(main_user) {
                strncat(userlist, response_T, sizeof(response_T));
                strncat(userlist, " ", 1);
                main_user = false;
            }
            
            if(!strcmp(response_T, "0\n")) {
                printf("The Central server received information from Backend-Server T using UDP over port %d.\n", SERVER_T_UDP_PORT);
                break;
            } else if(!strcmp(response_T, "\n")) { // the next string is the main user
                main_user = true;
            }
        }
        /***** Phase 3-1: Server S, send main users' name to ask for their scores from server S *****/
        // send data to request required users' scores
        char *tok = strtok(userlist, " ");
        while(tok != NULL) {
            if( sendto(udp_socket_fd, tok, 256, 0, (struct sockaddr*)&server_addr[1], length) < 0 ) {
                perror("write to server error!\n");
                exit(1);
            }
            tok = strtok(NULL, " ");
        }
        sendto(udp_socket_fd, "0", 1, 0, (struct sockaddr*)&server_addr[1], length);
        printf("The Central server sent a request to Backend-Server S\n");

        /***** Phase 3-2: Server S, recv scores from server S *****/
        char response_S[256];
        while(1) {
            memset(response_S, '\0', sizeof(response_S));
            // recv data from serverS
            if( recvfrom(udp_socket_fd, response_S, 256, 0, (struct sockaddr*)&server_addr[1], (socklen_t*)&length) < 0) {
                perror("read from server error\n");
                exit(1);
            }
            // printf("From Server S: %s\n", response_S);
            strncat(stored_data, response_S, 256);
            if(!strcmp(response_S, "1\n")) {
                printf("The Central server received information from Backend-Server S using UDP over port %d.\n", SERVER_S_UDP_PORT);
                break;
            }
        }

        /***** Phase 4-1: Server P, send username, topology and scores to server P *****/
        sendto(udp_socket_fd, username_A, 64, 0, (struct sockaddr*)&server_addr[2], length);
        sendto(udp_socket_fd, username_B, 64, 0, (struct sockaddr*)&server_addr[2], length);
        if( sendto(udp_socket_fd, stored_data, 1024, 0, (struct sockaddr*)&server_addr[2], length) < 0 ) {
            perror("write to server error!\n");
            exit(1);
        }
        printf("The Central server sent a processing request to Backend-Server P.\n");

        char result[256], matching_gap[64];
        memset(result, '\0', sizeof(result));
        memset(matching_gap, '\0', sizeof(matching_gap));
        /***** Phase 4-2: Server P, recv results from server P *****/
        if( recvfrom(udp_socket_fd, result, 256, 0, (struct sockaddr*)&server_addr[2], (socklen_t*)&length) < 0) {
            perror("read from server error\n");
            exit(1);
        }
        if( recvfrom(udp_socket_fd, matching_gap, 64, 0, (struct sockaddr*)&server_addr[2], (socklen_t*)&length) < 0) {
            perror("read from server error\n");
            exit(1);
        }
        printf("The Central server received the results from backend server P.\n");
        // printf("result: %s, matching gap: %s\n", result, matching_gap);

        /***** Phase 5-1: Client A, retransmit results from server P *****/
        if( send(tcp_socket_a_fd, username_B, 64, 0) < 0 ) {
            perror("send failed!\n");
            exit(1);
        }
        if( send(tcp_socket_a_fd, result, 256, 0) < 0 ) {
            perror("send failed!\n");
            exit(1);
        }
        if( send(tcp_socket_a_fd, matching_gap, 64, 0) < 0 ) {
            perror("send failed!\n");
            exit(1);
        }
        close(tcp_socket_a_fd);
        printf("The Central server sent the results to client A.\n");

        /***** Phase 5-2: Client B, retransmit results from server P *****/
        if( send(tcp_socket_b_fd, username_A, 64, 0) < 0 ) {
            perror("send failed!\n");
            exit(1);
        }
        if( send(tcp_socket_b_fd, result, 256, 0) < 0 ) {
            perror("send failed!\n");
            exit(1);
        }
        if( send(tcp_socket_b_fd, matching_gap, 64, 0) < 0 ) {
            perror("send failed!\n");
            exit(1);
        }
        close(tcp_socket_b_fd);
        printf("The Central server sent the results to client B.\n");

    }
    // close(udp_socket_fd);
    return 0;
}