#include <stdio.h>
#include <stdlib.h>

const int MAX_ACCEPT_SOCK_COUNT = 10;
const int SERVER_C_UDP_PORT = 24091;
const int SERVER_C_TCP_PORT_A = 25091;
const int SERVER_C_TCP_PORT_B = 26091;
const int SERVER_T_UDP_PORT = 21091;
const int SERVER_S_UDP_PORT = 22091;
const int SERVER_P_UDP_PORT = 23091;

struct sockaddr_in server_addr[2];
int length = sizeof(server_addr[0]);

int udp_client_socket();
int tcp_server_socket_setup(int port);
int tcp_accept_client_sock(int server_socket_fd);