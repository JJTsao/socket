#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "clientB.h"

int main(int argc, char *argv[]) {
    if(argc < 2) {
        perror("Usage: ./client [username]");
        exit(1);
    }
    char *username = argv[1];
    printf("The client is up and running.\n");
    int port = SERVER_C_TCP_PORT_B;
    int socket_fd;
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd == -1) {
        perror("Socket Create Failed.\n");
        exit(1);
    }

    struct sockaddr_in server;
    memset(&server, '\0', sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(port);
    
    if( connect(socket_fd, (struct sockaddr*)&server, sizeof(server)) < 0 ) {
        perror("Connected Failed.\n");
        exit(1);
    }

    struct sockaddr_in local;
    memset(&local, '\0', sizeof(server));
    int len = sizeof(local);
    getsockname(socket_fd, (struct sockaddr*)&local, (socklen_t*)&len);
    char localIP[16];
    inet_ntop(AF_INET, &local.sin_addr, localIP, sizeof(localIP));

    if( send(socket_fd, username, strlen(username), 0) < 0 ) {
        perror("Send Failed.\n");
        exit(1);
    }
    printf("The client sent %s to the Central server.\n", username);

    char result[256], matching_gap[64], username_A[64];
    if( recv(socket_fd, username_A, 64, 0) < 0 ) {
        perror("recv Failed.\n");
        exit(1);
    } 
    if( recv(socket_fd, result, 256, 0) < 0 ) {
        perror("recv Failed.\n");
        exit(1);
    } 
    if( recv(socket_fd, matching_gap, 64, 0) < 0 ) {
        perror("recv Failed.\n");
        exit(1);
    }
    
    // printf("result: %s, matching gap: %s\n", result, matching_gap);
    if(result == NULL || !strcmp(matching_gap, "0")) { printf("Found no compatibility for %s and %s\n", username, username_A); }
    else {
        printf("Found compatibility for %s and %s:\n", username, username_A); 
        char* substr = strtok(result, " ");
        printf("%s", substr);
        substr = strtok(NULL, " ");
        while(substr != NULL) {
            printf(" --- ");
            printf("%s", substr);
            substr = strtok(NULL, " ");
        }
        printf("\nMatching Gap : %s\n", matching_gap);
    } 

    close(socket_fd);
    return 0;
}