# EE450 Socket Programming Project, Fall

## a. JJTsao
## b. 
## c. What you have done in the assignment, if you have completed the optional part (suffix). If it’s not mentioned, it will not be considered.
[Done] Phase 1A
[Done] Phase 1B
[Done] Phase 2
[Done] Phase 3
[Not done] Phase 4

## d. What your code files are and what each one of them does. (Please do not repeat the project description, just name your code files and briefly mention what they do).
### Project Tree
```
student@studentVM:~$ tree socket/
socket/
├── central
│   ├── central.c
│   ├── central.h
│   └── topo_scores.txt
├── clientA.c
├── clientA.h
├── clientB.c
├── clientB.h
├── makefile
├── README.md
├── serverP
│   ├── serverP.c
│   └── serverP.h
├── serverS
│   ├── scores.txt
│   ├── serverS.c
│   └── serverS.h
└── serverT
    ├── edgelist.txt
    ├── serverT.c
    └── serverT.h

4 directories, 17 files
```
### Overall
+ udp & tcp socket functions:
    + ip, port and other settings
    + return the socket fd

### Central Server
**Handling the message transfer between Server T, S, P & clients.**
+ central.h: declaration of some ports for socket to use, and socket setting functions.
+ central.c: message receiving & retransmit to new targets.
    + I learned most socket building knowledge in implementing this server.

### Server T: serverT.c serverT.h
**Use DFS to get the connected component of network topology including user A & B.**
+ serverT.h: declaration of the structure, global variables & functions.
    + struct UserNode:
        I implement an adjacency list using linked list, which UserNode includes a *struct user* represents the main user and the pointer *next* point to the next main user.
    + struct User:
        A struct User includes the user's name and there's a pointer neighbor to record other users that is adjacent to the main user.
    + struct Vertex:
        A struct be used to record whether a vertex(user) has been visited in DFS.

+ serverT.c: implement of the needed functions, socket building and data parsing.
    + The main algorithm in serverT is DFS, which is used to get the connected component of network topology including user A & B.
    + Then record the result by concatenate the users' name in a string(char*) as data to send to Central server.

### Server S
**Read the score from text file and response to Central server's request.**
+ serverS.h: declaration of the structure, global variables & functions.
    + struct User:
        I implement an linked list *user_list*, for record the users' scores which is read from scores.txt.

+ serverS.c: implement of the needed functions, socket building and data parsing.
    + The easiest server, nothing special.

### Server P
**Parse the data received and find the path between userA & userB with BFS, then calculate the matching gap along the path to return to Central server.**
+ serverP.h: declaration of the structure, global variables & functions.
    + Lots of functions shared with serverT & serverS to store the data into proper data structure.
    + Implement a simple Queue structure & needed functions for BFS
    + BFS algorithm
    + Handling if userA & userB is compatible
    + Calculate the matching gap

+ serverP.c: implement of the needed functions, socket building and data parsing.
    + The hardest server, the data parsing takes a lot of time.
    + implementation of functions mentioned in serverP.h

### Client A / B
**Send the username and wait for Central server's response, then display the results.**
+ clientA.h, clientA.c, clientB.h, clientB.c 
+ Nothing special, the only difference between clientA & clientB is the display of users on the path, which are shown in reverse form.

e. The format of all the messages exchanged.
### myPhase 1: ClientA & ClientB    -> Central server 
+ char username\_A[64], username\_B[64]
### myPhase 2: Central server       -> serverT 
+ char username\_A[64]
### myPhase 3: serverT              -> Central server
+ char response_T[256]
    + adjacency list represent in pure character pointer(array)
    + a folloing "0" to mark the end of message from serverT
### myPhase 4: Central server       -> serverS
+ char *tok // usernames
    + tokenize the main users' name to request for their score from serverS
### myPhase 5: serverS              -> Central server
+ char response_S[256]
    + the required user and their scores in pure character array
### myPhase 6: Central server       -> serverP
+ char stored_data[1024]
    + concatenate the response from serverT & serverS in pure character array to send to serverP
### myPhase 7: serverP              -> Central server
+ char result[256], matching_gap[64]
    + result is the path found between userA & userB (in character array)
    + matching\_gap is matching\_gap but in string type
### myPhase 8: Central server       -> ClientA & ClientB
+ char username[64], result[256], matching_gap[64]
    + send the other username to each of them (for display message)
    + result, matching_gap are same as myPhase 7

g. Any idiosyncrasy of your project. It should say under what conditions the project fails, if any.
1. Most of data structure I use is linked list, so it's bad efficiency in searching exact element I need.
2. I assume the length of username were all less than 64 character, so if the test input is longer, my program may failed.
3. Similiar as 2., I assume the messages transfer between servers & clients were less than 256 character.

h. Reused Code: Did you use code from anywhere for your project? If not, say so. If so, say what functions and where they're from. (Also identify this with a comment in the source code.)
**I didn't copy or directly use code from others, but here's some reference helping me implement my own functions.**
## References
// socket
https://www.binarytides.com/server-client-example-c-sockets-linux/
https://gist.github.com/listnukira/4045436
http://zake7749.github.io/2015/03/17/SocketProgramming/
http://stenlyho.blogspot.com/2008/08/udp-serverclient.html
// linked list
http://alrightchiu.github.io/SecondRound/linked-list-xin-zeng-zi-liao-shan-chu-zi-liao-fan-zhuan.html
// BFS
https://alrightchiu.github.io/SecondRound/graph-breadth-first-searchbfsguang-du-you-xian-sou-xun.html
// implement queue with array
http://alrightchiu.github.io/SecondRound/queue-yi-arrayshi-zuo-queue.html