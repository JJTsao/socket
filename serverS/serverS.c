#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdbool.h>
#include "serverS.h"

int udp_server_socket() {
    int socket_fd;
    struct sockaddr_in server;

    if( (socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        perror("Socket Created Failed\n");
        exit(1);
    }
    // printf("Socket Created Success.\n");

    memset(&server, '\0', sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_port = htons(SERVER_S_UDP_PORT);

    if( bind(socket_fd, (struct sockaddr*)&server, sizeof(server)) < 0 ) {
        perror("Socket Binded Failed\n");
        exit(1);
    }
    // printf("Socket Binded Success.\n");

    return socket_fd;
}

int main() {
    FILE *fp;
    fp = fopen("/home/student/socket/serverS/scores.txt", "r");
    if(fp == NULL) {
        perror("fopen failed.");
        exit(1);
    }
    int USER_COUNT = 0;
    FILE *tmpfp = fopen("/home/student/socket/serverS/scores.txt", "r"); // count the lines
    while(!feof(tmpfp)) {
        char ch = fgetc(tmpfp);
        if(ch == '\n') { USER_COUNT++; }
    }

    user_list = malloc(USER_COUNT * sizeof(struct User));
    if(user_list == NULL) {
        perror("Unable to allocate required memory!");
        exit(1);
    }
    
    char arr[64];
    int i=0;
    while( fgets(arr, 64, fp) != NULL ) {
        char *substr = NULL;
        struct User tmp;
        substr = strtok(arr, " ");
        strncpy(tmp.name, substr, 64);
        substr = strtok(NULL, "\n");
        tmp.score = atoi(substr);
        user_list[i] = tmp; 
        i++;
    }

    int server_s_socket_fd = udp_server_socket();
    printf("The ServerS is up and running using UDP on port %d.\n", SERVER_S_UDP_PORT);
    
    while(1) {
        char buf[256];
        struct sockaddr_in client_addr;
        int length = sizeof(client_addr);
        bool first_req = true;
        while(1) {
            memset(buf, '\0', sizeof(buf));
            if( recvfrom(server_s_socket_fd, &buf, 256, 0, (struct sockaddr*)&client_addr, (socklen_t*)&length) < 0 ) {
                perror("couldn't read datagram\n");
                exit(1);
            }
            if(first_req) { printf("The ServerS received a request from Central to get the scores.\n"); first_req = false; }

            if( !strcmp(buf, "0") ) break;
            for(int i=0; i<USER_COUNT; i++) {
                if(!strcmp(buf, user_list[i].name)) {
                    sendto(server_s_socket_fd, user_list[i].name, 64, 0, (struct sockaddr*)&client_addr, length);
                    sendto(server_s_socket_fd, " ", 1, 0, (struct sockaddr*)&client_addr, length);
                    char score[64];
                    memset(score, '\0', sizeof(score));
                    sprintf(score, "%d", user_list[i].score);
                    sendto(server_s_socket_fd, score, 64, 0, (struct sockaddr*)&client_addr, length);
                    sendto(server_s_socket_fd, "\n", 1, 0, (struct sockaddr*)&client_addr, length);
                }
            }
        }
        sendto(server_s_socket_fd, "1\n", 2, 0, (struct sockaddr*)&client_addr, length);
        printf("The ServerS finished sending the scores to Central.\n");

        // close(server_s_socket_fd);
    }
    return 0;
}