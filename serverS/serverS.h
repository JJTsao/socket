#include <stdio.h>

const int SERVER_T_UDP_PORT = 21091;
const int SERVER_S_UDP_PORT = 22091;
const int SERVER_P_UDP_PORT = 23091;

struct User {
    char name[64];
    int score;
};

struct User *user_list;

int udp_server_socket();
